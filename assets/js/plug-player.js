var plugPlayer;

$(function () {
    mediumScreenRender();
});

$(window).on('resize', function(){
    mediumScreenRender();

    if (!_.isUndefined()) {
        plugPlayer.renderVolumeUI();
        plugPlayer.updateUI();
    }
});

$('#plugPlayer').find('.disabled-container').click(function() {
    loadPlugPlayer();
    $('#plugPlayer').find('.loading-container').fadeIn('fast', function () {
        $('#plugPlayer').find('.disabled-container').fadeOut();
    });
});

function loadPlugPlayer() {
    $.ajax({
        url: '/random-tracks',
        type: 'GET',
        success: function (data) {
            plugPlayer = new PlugPlayer(data);
            plugPlayer.init();
            bindControls();
        },
        dataType: 'json'
    });
}

function mediumScreenRender () {
    if ($(window).width() <= 767) {
        $('.plug-player-container').parent().insertAfter($('.navbar-container'));
    }
}

function bindControls() {
    plugPlayer.player.find('.play-button').click(function () {
        plugPlayer.play();
    });

    plugPlayer.player.find('.pause-button').click(function () {
        plugPlayer.pause();
    });

    plugPlayer.player.find('.previous-button').click(function () {
        plugPlayer.previous();
    });

    plugPlayer.player.find('.next-button').click(function () {
        plugPlayer.next();
    });

    plugPlayer.player.find('.progress').click(function (e) {
        var positon = e.pageX - $(this).offset().left;

        if (positon < 0) positon = 0;

        var positionPercent = 100 * parseInt(positon, 10) / $(this).width();
        plugPlayer.setSeek(plugPlayer.getDuration() * positionPercent / 100);
        $(this).find('.progress-bar').width(positionPercent + '%');
    });
}

function updateProgressBar() {
    plugPlayer.player.find('.progress-bar').width(100 * plugPlayer.setSeek() / plugPlayer.getDuration() + '%');
}

var PlugPlayer = function (tracks) {

    this.player = $('#plugPlayer');
    this.tracks = tracks;
    this.currentTrackIndex = 0;
    this.progressBarInterval = null;
    this.isPlaying = false;
    this.hasPrevious = false;
    this.hasNext = false;
    this.retrievingTracks = false;

    this.init = function () {
        if (_.isEmpty(this.tracks) || _.isEmpty(this.tracks[0])) {
            console.log('PlugPlayer unable to l\'load tracks');
            return;
        }

        _.forEach(this.tracks, function (track) {
            track.howl = new Howl({
                src: track.fileUrl,
                html5: true,
                preload: false
            });
        });

        this.renderVolumeUI();
        this.updateUI();

        this.player.find('.loading-container').fadeOut('slow');
        this.play();
    };

    this.moreTracks = function () {
        plugPlayer.retrievingTracks = true;

        $.ajax({
            url: '/random-tracks',
            type: 'GET',
            success: function (tracks) {
                _.forEach(tracks, function (track) {
                    track.howl = new Howl({
                        src: track.fileUrl,
                        html5: true,
                        preload: false
                    });
                });

                plugPlayer.tracks = _.concat(plugPlayer.tracks, tracks);
                plugPlayer.checkNavigation();
            },
            complete: function () {
                plugPlayer.retrievingTracks = false;
            },
            dataType: 'json'
        });
    };

    this.play = function () {
        var howl = this.getCurrentTrack().howl;

        if (howl.state() === 'unloaded') {
            howl.load();
            this.player.find('.play-button').parent().hide();
            this.player.find('.pause-button').parent().hide();
            this.player.find('.loading-icon').parent().show();
            howl.on('load', function () {
                plugPlayer.player.find('.loading-icon').parent().hide();

                if (plugPlayer.isPlaying)
                    plugPlayer.player.find('.pause-button').parent().show();
                else
                    plugPlayer.player.find('.play-button').parent().show();
                plugPlayer.play();
            });
            return;
        }

        if (_.isEmpty(this.progressBarInterval)) {
            this.progressBarInterval = setInterval(updateProgressBar, 1000);
        }

        howl.play();
        howl.off('end');
        howl.on('end', function () {
            plugPlayer.player.find('.pause-button').parent().hide();
            plugPlayer.player.find('.play-button').parent().show();
            plugPlayer.next();
        });

        this.isPlaying = true;
        this.player.find('.play-button').parent().hide();
        this.player.find('.pause-button').parent().show();
    };

    this.pause = function () {
        var howl = this.getCurrentTrack().howl;

        howl.pause();
        this.isPlaying = false;
        this.player.find('.pause-button').parent().hide();
        this.player.find('.play-button').parent().show();
    };

    this.stop = function () {
        var howl = this.getCurrentTrack().howl;

        howl.stop();
        this.progressBarInterval = null;
        this.player.find('.pause-button').parent().hide();
        this.player.find('.play-button').parent().show();
        this.player.find('.progress-bar').width('0px');
    };

    this.previous = function () {
        if (!this.hasPrevious)
            return;

        var howl = this.getCurrentTrack().howl;

        if (howl.state() === 'loading') {
            howl.unload();
            plugPlayer.player.find('.loading-icon').parent().hide();
            plugPlayer.player.find('.play-button').parent().show();
        }

        this.stop();
        this.currentTrackIndex--;
        this.updateUI();

        if (this.isPlaying)
            this.play();
    };

    this.next = function () {
        if (!this.hasNext)
            return;

        if (!this.retrievingTracks && this.currentTrackIndex >= this.tracks.length-3) {
            this.moreTracks();
        }

        var howl = this.getCurrentTrack().howl;

        if (howl.state() === 'loading') {
            howl.unload();
            plugPlayer.player.find('.loading-icon').parent().hide();
            plugPlayer.player.find('.play-button').parent().show();
        }

        this.stop();
        this.currentTrackIndex++;
        this.updateUI();

        if (this.isPlaying)
            this.play();
    };

    this.checkNavigation = function () {
        if (this.currentTrackIndex === 0) {
            this.hasPrevious = false;
            this.player.find('.previous-button').addClass('disabled');
        }
        else {
            this.hasPrevious = true;
            this.player.find('.previous-button').removeClass('disabled');
        }

        if (this.currentTrackIndex === this.tracks.length-1) {
            this.hasNext = false;
            this.player.find('.next-button').addClass('disabled');
        }
        else {
            this.hasNext = true;
            this.player.find('.next-button').removeClass('disabled');
        }
    };

    this.updateUI = function () {
        var currentTrack = this.getCurrentTrack();

        this.player.find('.mixtape-cover').attr('src', currentTrack.mixtape.coverUrl);
        this.player.find('.mixtape-title').html(currentTrack.mixtape.title);
        this.player.find('.mixtape-link').attr('href', '/mixtape/' + currentTrack.mixtape.slug);

        if ($(window).width() <= 767) {
            this.player.find('.mixtape-title').parent().removeAttr('href');
        }

        var trackTitle = "";
        var trackArtist = "";

        if (!_.isUndefined(currentTrack.artist) || !_.isUndefined(currentTrack.musicGroup)) {
            trackArtist = !_.isEmpty(currentTrack.artist) ? currentTrack.artist.name : currentTrack.musicGroup.name;
        }

        if (!_.isEmpty(trackArtist)) {
            trackTitle = trackArtist + ' - ';
        }

        trackTitle += currentTrack.title;

        var featurings = _.concat(currentTrack.featuringArtists, currentTrack.featuringMusicGroups);

        if (!_.isEmpty(featurings)) {
            trackTitle += ' ft. ';

            _.forEach(featurings, function (featuring) {
                trackTitle += featuring.name + ', ';
            });

            trackTitle = trackTitle.substring(0, trackTitle.length - 2);
        }

        if (!_.isEmpty(currentTrack.beatmakers)) {
            trackTitle += ' Prod. ';

            _.forEach(currentTrack.beatmakers, function (beatmaker) {
                trackTitle += beatmaker.name + ', ';
            });

            trackTitle = trackTitle.substring(0, trackTitle.length - 2);
        }

        this.player.find('.track-title').html(trackTitle);
        this.player.find('.track-title').ellipsis();
        this.player.find('.mixtape-title').ellipsis();

        this.checkNavigation();
    };

    this.renderVolumeUI = function () {
        var stepsCount = $('.media-player-volume-container').width()/6;
        var volume = new VolumeControl('#plugPlayer .volume-slider', stepsCount);
        volume.renderUI(VolumeControl.getDefaultVolumeValue());
    };

    this.getCurrentTrack = function () {
        return this.tracks[this.currentTrackIndex];
    };

    this.getDuration = function () {
        return this.getCurrentTrack().howl.duration();
    };

    this.getSeek = function () {
        return this.getCurrentTrack().howl.seek();
    };

    this.setSeek = function (seek) {
        return this.getCurrentTrack().howl.seek(seek);
    };
};