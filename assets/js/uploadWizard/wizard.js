var mixtape = {
    musicGenres: [],
    artists: [],
    tracks: []
};

$(document).ready(function () {
    $(".artist-select-container .bootstrap-select .dropdown-menu.open").prepend("<div class='artist-missing'><a href='#' data-toggle='modal' data-target='#createArtistModal' data-backdrop='static' data-keyboard='false'>Ajouter un artiste</a></div>");

    var navListItems = $('.setup-panel .upload-wizard-step a'),
        allWells = $('.setup-content'),
        previousButtons = $('.previous-button');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    previousButtons.click(function () {
        var $currentStep = $(this).closest(".setup-content"),
            nextStepId = _.toNumber($currentStep.data('step')) - 1,
            $previousStep = $('.setup-content[data-step="' + nextStepId + '"]'),
            $previousStepNavItem = $('.upload-wizard-step a[data-step="' + nextStepId + '"]');

        navListItems.removeClass('btn-primary').addClass('btn-default');
        $previousStepNavItem.addClass('btn-primary');
        allWells.hide();
        $previousStep.show();
        $previousStep.find('input:eq(0)').focus();
    });

    $('#finalModal').find('.btn-default').click(function () {
        window.location.href = "/me/my-mixtapes";
    });

    $('.setup-content[data-step="1"]').show();
});

function nextStep(step) {
    var $currentStep = step.closest(".setup-content"),
        nextStepId = _.toNumber($currentStep.data('step')) + 1,
        $nextStep = $('.setup-content[data-step="' + nextStepId + '"]'),
        $nextStepNavItem = $('.upload-wizard-step a[data-step="' + nextStepId + '"]');

    $('.setup-panel .upload-wizard-step a').removeClass('btn-primary').addClass('btn-default');
    $nextStepNavItem.addClass('btn-primary');
    $('.setup-content').hide();
    $nextStep.show();
    $nextStep.find('input:eq(0)').focus();
}

function createMixtape() {
    $('.wizard-pagination .btn-success').prop('disabled', true);

    $.ajax({
        url: '/mixtape/create',
        type: 'POST',
        data: mixtape,
        success: function(created) {
            var modal = $('#finalModal');

            $(window).unbind("beforeunload");

            modal.modal({
                backdrop: 'static',
                keyboard: false
            });

            if (created.isOfficial) {
                modal.find('.mixtape-confirmation-code').html(created.confirmationCode);
                modal.find('.official-release-confirmation-info').show();
            }
        },
        dataType: 'json'
    });
}