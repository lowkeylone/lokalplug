var filters = {};
var tracklistPlayer;
var currentlyPlayingProgress;
var playingMixtape = false;
var playingMixtapeIndex = 0;

$(function () {
    $('.list-filters .default-filter').each(function (index, element) {
        filters[$(element).attr("data-filter")] = $(element).attr("data-filter-currentValue");
    });

    var mixtapeTracklist = $('.mixtape-tracklist');
    if (!_.isEmpty(mixtapeTracklist))
        tracklistPlayer = new TracklistPlayer(mixtapeTracklist);
});

$('.search-button').click(function (e) {
    e.preventDefault();

    var navigation = $('.navbar-nav');
    var largeForm = $('.navbar-large-form');

    navigation.removeClass('visible-sm visible-md visible-lg');
    navigation.fadeOut('fast', function () {
        if (!largeForm.is(":visible")) {
            largeForm.fadeIn('fast');
            largeForm.find('input').focus();
        }
    });
});

$('.navbar-large-form input').focusout(function () {
    var navigation = $('.navbar-nav');
    var largeForm = $('.navbar-large-form');

    largeForm.fadeOut('fast', function () {
        if (!navigation.is(':visible')) {
            navigation.fadeIn('fast');
            $('.navbar .navbar-right').addClass('visible-sm visible-md visible-lg');
        }
    });
});

$('.list-filters .filter').click(function () {
    filters[$(this).attr("data-filter")] = $(this).attr("data-filter-value");

    var uri = URI(window.location.href);

    _.forEach(filters, function (value, filter) {
        uri.removeSearch(filter);

        if (!_.isEmpty(value))
            uri.addSearch(filter, value);
    });

    window.location.href = uri.toString();
});

$('.list-sort-dropdowns .sort-filter').click(function () {
    var uri = URI(window.location.href);

    uri.removeSearch($(this).attr("data-attribute"));

    if (!_.isEmpty($(this).attr("data-sort")))
        uri.addSearch($(this).attr("data-attribute"), $(this).attr("data-sort"));

    window.location.href = uri.toString();
});

$('.language-link').click(function (e) {
    e.preventDefault();
    $.post("/language/" + $(this).data('language'), function () {
        location.reload();
    });
});

function createUser() {
    $.post("/signup", $("#signup-form").serialize(), function () {
        $("#signup-form").fadeOut("fast");
        $("#signup-form-success").fadeIn("fast");
    }).fail(function () {
        $("#signup-form").fadeOut("fast");
        $("#signup-form-failed").fadeIn("fast");
    });
}

function loggingUser() {
    $.post("/login", $("#login-form").serialize(), function () {
        $("#login-form-success").fadeIn("fast");
        window.location.href = "/me";
    }).fail(function () {
        $("#login-form-failed").fadeIn("fast");
    }).always(function () {
        $("#login-form").get(0).reset();
        window.grecaptcha.reset();
    });
}

function sendContactForm() {
    var contactForm = $('#contactForm');
    var name = contactForm.find('#name');
    var email = contactForm.find('#email');
    var subject = contactForm.find('#subject');
    var message = contactForm.find('#message');

    if (!_.isEmpty(name.val()) && !_.isEmpty(email.val()) && !_.isEmpty(subject.val()) && !_.isEmpty(message.val())) {
        $.post("/contact", contactForm.serialize(), function () {
            $('.contact-form-container .alert-success').show();
        }).fail(function () {
            $('.contact-form-container .alert-danger').show();
        }).always(function () {
            contactForm.hide();
        });
    }
}

function notify(type, message) {
    var navbar = $('.navbar-container');
    var navEndOffsetTop = navbar.offset().top + navbar.height();
    var navEndRelativeTop = navEndOffsetTop - $(window).scrollTop();

    var notifyOffsetTop = navEndRelativeTop > 20 ? navEndRelativeTop + 20 : 20;

    $.notify({
        message: message
    }, {
        type: type,
        delay: 3000,
        offset: {
            y: notifyOffsetTop,
            x: 20
        }
    });
}

function notifyDanger(message) {
    notify('danger', message);
}

function notifySuccess(message) {
    notify('success', message);
}

function deleteMixtape(slug, cb) {
    $.ajax({
        url: '/mixtape/' + slug + '/delete',
        type: 'DELETE',
        success: function () {
            notifySuccess('Mixtape supprimée');
        },
        error: function () {
            notifyDanger('Une erreur est survenue');
        },
        complete: cb
    });
}

function deleteArtist(slug, cb) {
    $.ajax({
        url: '/artist/' + slug + '/delete',
        type: 'DELETE',
        success: function () {
            notifySuccess('Artiste supprimé')
        },
        error: function () {
            notifyDanger('Une erreur est survenue');
        },
        complete: cb
    });
}

function deleteBeatmaker(slug, cb) {
    $.ajax({
        url: '/beatmaker/' + slug + '/delete',
        type: 'DELETE',
        success: function () {
            notifySuccess('Beatmaker supprimé')
        },
        error: function () {
            notifyDanger('Une erreur est survenue');
        },
        complete: cb
    });
}

function deleteMusicGroup(slug, cb) {
    $.ajax({
        url: '/music-group/' + slug + '/delete',
        type: 'DELETE',
        success: function () {
            notifySuccess('Groupe supprimé')
        },
        error: function () {
            notifyDanger('Une erreur est survenue');
        },
        complete: cb
    });
}

$('#uploadAvatar').click(function (e) {
    e.preventDefault();

    $('.loading-container').fadeIn('slow');

    $.ajax({
        url: '/me/settings/update-avatar',
        type: 'POST',
        data: new FormData($('#avatarSettingsForm')[0]),
        processData: false,
        contentType: false,
        success: function () {
            location.reload();
        },
        error: function () {
            notifyDanger(anErrorOccurredAvatarMsg);
        },
        complete: function () {
            $('.loading-container').fadeOut('slow');
            $('#avatarSettingsForm')[0].reset();
        }
    });
});

$('#deleteAvatar').click(function (e) {
    e.preventDefault();

    $('.loading-container').fadeIn('slow');

    $.get('/me/settings/delete-avatar')
        .done(function () {
            location.reload();
        })
        .fail(function () {
            notifyDanger(anErrorOccurredMsg);
        })
        .always(function () {
            $('.loading-container').fadeOut('slow');
        });
});

$('#basicSettingsForm').validator().on('submit', function (e) {
    if (!e.isDefaultPrevented()) {
        e.preventDefault();

        $.post("/me/settings/basic-update", $(this).serialize(), function () {
            location.reload();
        }).fail(function () {
            notifyDanger(anErrorOccurredPasswordMsg);
        });
    }
});

$('#passwordSettingsForm').validator().on('submit', function (e) {
    if (!e.isDefaultPrevented()) {
        e.preventDefault();

        $.post("/me/settings/update-password", $(this).serialize(), function () {
            notifySuccess(passwordUpdateSuccessMsg);
        }).fail(function () {
            notifyDanger(anErrorOccurredPasswordMsg);
        }).always(function () {
            $('#passwordSettingsForm')[0].reset();
        });
    }
});

$('#resendEmailConfirmation').click(function () {
    $.get('/me/settings/resend-email-confirmation', function () {
        notifySuccess(mailConfirmationSentMsg);
    });
});

$('#cancelEmailUpdate').click(function () {
    $.get('/me/settings/cancel-email-update', function () {
        $('#confirmEmailWarning').hide(400);
        notifySuccess(mailUpdateCancelledMsg);
    });
});

$('.download-track').click(function () {
    top.location.href = $(this).attr('href');
});

$('.like-mixtape').click(function (e) {
    e.preventDefault();

    $.ajax({
        url: '/mixtape/' + mixtapeSlug + '/like',
        type: 'POST',
        success: function (data) {
            $('.dislike-mixtape').removeClass('active');

            if (data.isLiking) {
                $('.like-mixtape').addClass('active');
            } else {
                $('.like-mixtape').removeClass('active');
            }

            $('.mixtape-rate').html(data.rate);
        },
        dataType: 'json'
    });
});

$('.dislike-mixtape').click(function (e) {
    e.preventDefault();

    $.ajax({
        url: '/mixtape/' + mixtapeSlug + '/dislike',
        type: 'POST',
        success: function (data) {
            $('.like-mixtape').removeClass('active');

            if (data.isDisliking) {
                $('.dislike-mixtape').addClass('active');
            } else {
                $('.dislike-mixtape').removeClass('active');
            }

            $('.mixtape-rate').html(data.rate);
        },
        dataType: 'json'
    });
});

$('.bookmark-mixtape').click(function (e) {
    e.preventDefault();

    $.ajax({
        url: '/mixtape/' + mixtapeSlug + '/bookmark',
        type: 'POST',
        success: function (data) {
            if (data.isBookmarking) {
                $('.bookmark-mixtape').addClass('active');
            } else {
                $('.bookmark-mixtape').removeClass('active');
            }
        },
        dataType: 'json'
    });
});

$('.play-all-tracks').click(function (e) {
    e.preventDefault();
    tracklistPlayer.stop();

    if (!playingMixtape) {
        $(this).addClass('active');
        playingMixtape = true;
        playingMixtapeIndex = 0;

        $('.mixtape-track').get(playingMixtapeIndex).click();
    } else {
        $(this).removeClass('active');
        playingMixtape = false;
        playingMixtapeIndex = 0;
    }
});

$('.mixtape-track').click(function () {
    var track = $(this);

    if (!track.hasClass('playing') && !track.hasClass('loading') && !track.hasClass('paused') && tracklistPlayer.isPlaying()) {
        tracklistPlayer.stop();
    }

    if (!track.hasClass('playing') && !track.hasClass('paused') && !track.hasClass('loading')) {
        tracklistPlayer.play(track.data('slug'));
        track.addClass('loading');
        renderTrackUI(track);
        currentlyPlayingProgress = setInterval(function () {
            updateTrackProgressBar(track)
        }, 1000);

    } else if (track.hasClass('playing') || track.hasClass('loading')) {
        tracklistPlayer.pause();
        track.removeClass('playing');
        track.addClass('paused');

    } else if (track.hasClass('paused')) {
        tracklistPlayer.play(track.data('slug'));
        track.removeClass('paused');
        track.addClass('playing');
    }

}).children().click(function (e) {
    var className = e.target.className;
    return !_.includes(className, 'volume') && !_.includes(className, 'track-volume-container') && !_.includes(className, 'fa-refresh') && !_.includes(className, 'progress') && !_.includes(className, 'download-track');
});

function updateTrackProgressBar(track) {
    track.find('.progress-bar').width(100 * tracklistPlayer.getSeek() / tracklistPlayer.getDuration() + '%');
}

$('.progress-bar-container').click(function (e) {
    var positon = e.pageX - $(this).offset().left;

    if (positon < 0) positon = 0;

    var positionPercent = 100 * parseInt(positon, 10) / $(this).width();
    tracklistPlayer.setSeek(tracklistPlayer.getDuration() * positionPercent / 100);
    $(this).find('.progress-bar').width(positionPercent + '%');
});

var TracklistPlayer = function (tracklist) {
    var player = this;

    player.tracks = {};

    _.forEach(tracklist.find('.mixtape-track'), function (track) {
        var slug = $(track).data('slug');

        $.ajax({
            url: '/track/' + slug + '/url',
            type: 'POST',
            success: function (data) {
                player.tracks[slug] = {
                    url: data.fileUrl,
                    howl: null
                }
            },
            dataType: 'json'
        });
    });

    this.isPlaying = function () {
        return !_.isUndefined(this.current);
    };

    this.play = function (slug) {
        var track = this.tracks[slug];

        if (!track.howl) {
            track.howl = new Howl({
                src: [track.url],
                html5: true,
                onplay: function () {
                    var trackElement = $('.mixtape-track[data-slug="' + slug + '"]');
                    trackElement.removeClass('loading');
                    trackElement.addClass('playing');
                },
                onend: function () {
                    var trackElement = $('.mixtape-track[data-slug="' + slug + '"]');
                    trackElement.removeClass('playing');
                    trackElement.addClass('paused');

                    if (playingMixtape) {
                        playingMixtapeIndex++;

                        var tracks = $('.mixtape-track');
                        if (!_.isUndefined(tracks.get(playingMixtapeIndex))) {
                            tracks.get(playingMixtapeIndex).click();
                        }
                        else {
                            $(this).removeClass('active');
                            playingMixtape = false;
                        }
                    }
                },
            });
        }

        this.tracks[slug].howl.play();
        this.current = slug;
    };

    this.pause = function () {
        this.tracks[this.current].howl.pause();
    };

    this.stop = function () {
        if (_.isUndefined(this.tracks[this.current]) || _.isUndefined(this.tracks[this.current].howl))
            return;

        this.tracks[this.current].howl.stop();

        $('.mixtape-tracklist').find('.mixtape-track').each(function () {
            $(this).removeClass('playing');
            $(this).removeClass('paused');
            $(this).removeClass('loading');
            $(this).find('.track-volume-container').empty();
            $(this).find('.progress-bar-container').hide();
            clearInterval(currentlyPlayingProgress);
        });
    };

    this.setSeek = function (seek) {
        this.tracks[this.current].howl.seek(seek);
    };

    this.getSeek = function () {
        return this.tracks[this.current].howl.seek();
    };

    this.getDuration = function () {
        return this.tracks[this.current].howl.duration();
    };
};