var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    Promise = require('bluebird'),
    bhttp = Promise.promisifyAll(require('bhttp'));

module.exports = {
    renderContactView: function(req, res) {

        var locals = {
            pageTitle: req.__('contactPage.title') + ' - Lokal Plug',
            section: 'contact'
        };

        return res.view('contact', locals);
    },

    sendContactMail: async(function(req, res) {
        var recaptchaConfirmed = await(bhttp.post('https://www.google.com/recaptcha/api/siteverify', {
            secret: sails.config.recaptchaSecret,
            response: req.body['g-recaptcha-response'],
            remoteip: req.ip
        })).body.success;

        if (!recaptchaConfirmed || _.isEmpty(req.body.name) || _.isEmpty(req.body.subject) || _.isEmpty(req.body.email) || _.isEmpty(req.body.message))
            return res.badRequest();

        var locals = {
            name: req.body.name,
            subject: req.body.subject,
            email: req.body.email,
            message: req.body.message
        };

        mailService.compileTemplate('contactRequest', locals, function (err, body) {
            mailService.sendNoReplyMail('support@lokalplug.com', '"Demande de contact" de '+ locals.name +' - Lokal Plug', body);

            if (err) {
                return res.serverError();
            }

            return res.ok();
        });
    })
};