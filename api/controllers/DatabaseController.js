var backup = require('s3-mongo-backup'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await');

module.exports = {
    backupToS3: async(function (req, res) {
        if (!await(apiService.checkAdminKey(req.query.key)))
            return res.forbidden();

        var backupConfig = {
            mongodb: {
                "database": "lokalplug",
                "host": "localhost",
                "username": "",
                "password": "",
                "port": 27017
            },
            s3: {
                accessKey: sails.config.s3Key,  //AccessKey
                secretKey: sails.config.s3Secret,  //SecretKey
                region: "us-east-1",     //S3 Bucket Region
                accessPerm: "private", //S3 Bucket Privacy, Since, You'll be storing Database, Private is HIGHLY Recommended
                bucketName: sails.config.s3Bucket //Bucket Name
            },
            keepLocalBackups: false,  //If true, It'll create a folder in project root with database's name and store backups in it and if it's false, It'll use temporary directory of OS
            noOfLocalBackups: 5, //This will only keep the most recent 5 backups and delete all older backups from local backup directory
            timezoneOffset: 300 //Timezone, It is assumed to be in hours if less than 16 and in minutes otherwise
        };

        backup(backupConfig).then(function () {
            return res.ok();
        }, function () {
            return res.serverError();
        });
    })
};