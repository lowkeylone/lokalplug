var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment'),
    slugify = require('slug'),
    URI = require('urijs');

module.exports = {
    renderAllBeatmakersView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var locals = {
            pageTitle: req.__('beatmakersPage.title') + ' - Lokal Plug',
            section: 'beatmaker',
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment
        };

        var data = {};

        data.totalPages = parseInt(await(Beatmaker.count()) / 24) + 1;
        data.currentPage = req.query.page || 1;
        data.previous = data.currentPage - 1 > 0;
        data.next = data.currentPage < data.totalPages;

        if (data.currentPage > data.totalPages) {
            var url = new URI(req.url).removeSearch('page').addSearch('page', data.totalPages);
            return res.redirect(url.toString());
        }

        data.results = await(Beatmaker.find({lastOfficialActivityAt: {'!': null}})
            .sort('lastOfficialActivityAt DESC')
            .paginate({page: data.currentPage, limit: 24})
            .populate('ownMixtapes'));

        _.forEach(data.results, function (beatmaker, index) {
            var rate = 0;

            _.forEach(beatmaker.ownMixtapes, function (mixtape) {
                rate = rate + mixtape.rate;
            });

            beatmaker.rate = rate;

            data.results[index] = beatmaker;
        });

        data.results = _.sortBy(data.results, [function (beatmaker) {
            return moment(beatmaker.lastOfficialActivityAt).valueOf();
        }, "rate"]).reverse();

        locals.data = data;

        return res.view('beatmakers', locals);
    }),

    renderBeatmakerView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var beatmaker = await(Beatmaker.findOne({slug: req.param('beatmakerSlug')}));

        if (_.isUndefined(beatmaker))
            return res.notFound();

        var section = 'beatmaker';

        var locals = {
            pageTitle: beatmaker.name + ' - Lokal Plug',
            section: section,
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment,
            artist: beatmaker,
            ownMixtapes: await(Mixtape.find({
                id: await(mixtapeService.getIdsByOwner(beatmaker.slug, 'beatmaker')),
                isConfirmed: true
            }).populate('artists').populate('beatmakers').populate('musicGroups').populate('musicGenres')),
            featuringMixtapes: await(Mixtape.find({
                id: await(mixtapeService.getIdsByFeaturing(beatmaker.slug, 'beatmaker')),
                isConfirmed: true
            }).populate('artists').populate('beatmakers').populate('musicGroups').populate('musicGenres'))
        };

        return res.view('artist', locals);
    }),

    renderAllBeatmakersAdminView: function (req, res) {
        var title = 'Tous les beatmakers';

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'allBeatmakers'
        };

        return res.view('accountAllBeatmakers', locals);
    },

    getAllBeatmakers: async(function (req, res) {
        moment.locale('fr');

        Beatmaker.find()
            .populate('musicGroups')
            .populate('owners')
            .skip(req.query.skip)
            .limit(req.query.limit)
            .sort(req.query.order_by + ' ' + req.query.sort)
            .exec(function (err, beatmakers) {
                _.forEach(beatmakers, function (beatmaker) {
                    beatmaker.lastOfficialActivityAt = moment(beatmaker.lastOfficialActivityAt).format('LL');
                    beatmaker.createdAt = moment(beatmaker.createdAt).format('LL');
                });

                if (err)
                    return res.serverError();

                return res.ok(beatmakers);
            });
    }),

    createBeatmaker: function (req, res) {
        req.body.slug = slugify(req.body.name, {lower: true});

        var musicGroupIds = req.body.musicGroups;
        _.unset(req.body, 'musicGroups');

        var beatmaker = await(Beatmaker.create(req.body));
        var adminUsers = await(User.find({isAdmin: true}));
        var musicGroups = await(MusicGroup.find({id: musicGroupIds}));

        _.forEach(adminUsers, function (admin) {
            admin.beatmakers.add(beatmaker.id);
            admin.save(function (err) {
            });
        });

        _.forEach(musicGroups, function (musicGroup) {
            musicGroup.beatmakers.add(beatmaker.id);
            musicGroup.save(function (err) {
            });
        });

        return res.ok();
    },

    deleteBeatmaker: async(function (req, res) {
        var beatmaker = await(Beatmaker.findOne({slug: req.param('beatmakerSlug')})
            .populate('tracks')
            .populate('ownMixtapes')
            .populate('featuringMixtapes')
            .populate('musicGroups')
            .populate('owners'));

        if (_.isEmpty(beatmaker))
            return res.notFound();

        if (!_.map(beatmaker.owners, 'id').includes(req.user.id) && !req.user.isAdmin)
            return res.badRequest();

        if (!_.isEmpty(beatmaker.ownMixtapes) || !_.isEmpty(beatmaker.featuringMixtapes) || !_.isEmpty(beatmaker.tracks))
            return res.badRequest('artist with id \'' + beatmaker.id + '\' has at least one relation with a mixtape, please delete it first');

        beatmakerService.removeBeatmakerRelations(beatmaker);

        Beatmaker.destroy({slug: req.param('beatmakerSlug')}).exec(function (err) {
            if (err)
                return res.serverError();

            return res.ok();
        });
    })
};