var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    Promise = require('bluebird'),
    genId = require('gen-id')('XXXXXXXX'),
    bhttp = Promise.promisifyAll(require('bhttp')),
    slugify = require('slug');

module.exports = {
    renderView: function (req, res) {
        var locals = {
            pageTitle: req.__('signupPage.title') + ' - Lokal Plug',
            section: 'other'
        };

        return res.view('signup', locals);
    },

    validateForm: async(function (req, res) {
        if (!_.isUndefined(req.query.username)) {
            var usersBySlug = await(User.count({slug: slugify(req.query.username, {lower: true})}));

            if (usersBySlug > 0) {
                res.writeHead(400, req.__('signupPage.form.usernameExistError'));
                return res.send();
            }

            if (req.query.username.length < 4) {
                res.writeHead(400, req.__('signupPage.form.usernameMinLengthError'));
                return res.send();
            }

            if (req.query.username.length > 20) {
                res.writeHead(400, req.__('signupPage.form.usernameMaxLengthError'));
                return res.send();
            }

            if (!/^[a-zA-Z0-9]+$/.test(req.query.username)) {
                res.writeHead(400, req.__('signupPage.form.usernameAlphaNumericError'));
                return res.send();
            }
        }

        if (!_.isUndefined(req.query.email)) {
            var usersByEmail = await(User.count({email: req.query.email}));

            if (usersByEmail > 0) {
                res.writeHead(400, req.__('signupPage.form.emailExistError'));
                return res.send();
            }
        }

        return res.ok();
    }),

    register: async(function (req, res) {
        var activationId = genId.generate();

        var recaptchaConfirmed = await(bhttp.post('https://www.google.com/recaptcha/api/siteverify', {
            secret: sails.config.recaptchaSecret,
            response: req.body['g-recaptcha-response'],
            remoteip: req.ip
        })).body.success;

        if (recaptchaConfirmed && await(formService.checkSignupFields(req.body))) {
            User.create({
                username: req.body.username,
                slug: slugify(req.body.username, {lower: true}),
                email: req.body.email,
                password: req.body.password,
                activationId: activationId
            }).exec(function (err) {
                if (err)
                    return res.serverError({signupStatus: 'failed', error: err});

                var mailLocals = {
                    __: req.__,
                    activationUrl: 'https://lokalplug.com/signup/activate/' + activationId,
                    accountUsername: req.body.username
                };

                mailService.compileTemplate('accountActivation', mailLocals, function (err, body) {
                    mailService.sendNoReplyMail(req.body.email, req.__('mails.accountActivation.subject'), body);
                });

                return res.ok({signupStatus: 'success'});
            });
        }
        else {
            return res.badRequest({signupStatus: 'failed', error: 'invalid form data'});
        }
    }),

    activateAccount: async (function(req, res) {
        var locals = {
            pageTitle: req.__('accountActivationPage.title') + ' - Lokal Plug',
            section: 'other'
        };

        var inactiveUser = await (User.findOne({activationId: req.params.activationId, active: false}));

        if(_.isEmpty(inactiveUser)) {
            locals.activationId = req.params.activationId;
            locals.activationStatus = 'fail';
        }
        else {
            await (User.update({id: inactiveUser.id}, {active: true}));
            locals.activationStatus = 'success';
        }

        return res.view('activateAccount', locals);
    })
};