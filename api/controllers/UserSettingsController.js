var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    slugify = require('slug'),
    uuidv1 = require('uuid/v1'),
    fileExt = require('file-extension'),
    path = require('path'),
    bcrypt = require('bcrypt'),
    os = require('os');

module.exports = {
    validateBasicSettingsForm: async(function (req, res) {
        if (!_.isUndefined(req.query.username)) {
            var usersBySlug = await(User.count({slug: slugify(req.query.username, {lower: true})}));

            if (usersBySlug > 0 && req.query.username !== req.user.username) {
                res.writeHead(400, req.__('signupPage.form.usernameExistError'));
                return res.send();
            }

            if (req.query.username.length < 4) {
                res.writeHead(400, req.__('signupPage.form.usernameMinLengthError'));
                return res.send();
            }

            if (req.query.username.length > 20) {
                res.writeHead(400, req.__('signupPage.form.usernameMaxLengthError'));
                return res.send();
            }

            if (!/^[a-zA-Z0-9]+$/.test(req.query.username)) {
                res.writeHead(400, req.__('signupPage.form.usernameAlphaNumericError'));
                return res.send();
            }
        }

        if (!_.isUndefined(req.query.email)) {
            var usersByEmail = await(User.count({email: req.query.email}));
            var usersByNewEmail = await(User.count({newEmail: req.query.email}));

            if ((usersByEmail > 0 && req.query.email !== req.user.email)
                || (usersByNewEmail > 0 && req.query.email !== req.user.newEmail)) {
                res.writeHead(400, req.__('signupPage.form.emailExistError'));
                return res.send();
            }
        }

        return res.ok();
    }),

    updateBasicSettings: async(function (req, res) {
        if (!await(bcrypt.compare(req.body.password, req.user.password))) {
            return res.badRequest();
        }

        if (await(formService.checkBasicSettingsFormFields(req.body, req.user))) {
            var fieldsToUpdate = {};

            if (req.body.username !== req.user.username) {
                fieldsToUpdate.username = req.body.username;
                fieldsToUpdate.slug = slugify(req.body.username, {lower: true})
            }

            if (req.body.email !== req.user.email) {
                fieldsToUpdate.newEmail = req.body.email;
                fieldsToUpdate.emailConfirmationId = uuidv1();
            }

            User.update({slug: req.user.slug}, fieldsToUpdate).exec(function (err, updated) {
                if (err)
                    return res.serverError();

                var user = updated[0];

                if (_.has(fieldsToUpdate, 'newEmail')) {
                    sails.controllers.usersettings.sendEmailConfirmationMail(res, user.newEmail, user.username, user.emailConfirmationId, req.__);
                }

                return res.ok();
            });
        }
        else {
            return res.badRequest();
        }
    }),

    resendEmailConfirmation: function (req, res) {
        if (_.isEmpty(req.user.newEmail) || _.isEmpty(req.user.emailConfirmationId))
            return res.badRequest();

        sails.controllers.usersettings.sendEmailConfirmationMail(res, req.user.newEmail, req.user.username, req.user.emailConfirmationId, req.__);
    },

    sendEmailConfirmationMail: function (res, receiver, username, confirmationId, i18n) {
        var locals = {
            __: i18n,
            accountUsername: username,
            confirmationUrl: 'https://lokalplug.com/confirm-email/' + confirmationId
        };

        mailService.compileTemplate('confirmNewEmail', locals, function (err, body) {
            mailService.sendNoReplyMail(receiver, i18n('mails.emailConfirmation.subject') + ' - Lokal Plug', body);

            if (err) {
                return res.serverError();
            }

            return res.ok();
        });
    },

    cancelEmailUpdate: async(function (req, res) {
        await(User.update({slug: req.user.slug}, {newEmail: '', emailConfirmationId: ''}));

        return res.ok();
    }),

    confirmEmail: async(function (req, res) {
        var locals = {
            pageTitle: req.__('confirmEmailPage.title') + ' - Lokal Plug',
            section: 'other'
        };

        var user = await(User.findOne({emailConfirmationId: req.params.confirmationId}));

        if (_.isEmpty(user)) {
            locals.confirmationId = req.params.confirmationId;
            locals.confirmationStatus = 'fail';
        } else {
            await(User.update({id: user.id}, {email: user.newEmail, newEmail: '', emailConfirmationId: ''}));
            locals.confirmationStatus = 'success';
        }

        return res.view('confirmEmail', locals);
    }),

    uploadNewAvatar: function (req, res) {
        if (!req._fileparser.upstreams.length)
            return res.badRequest();

        req.setTimeout(5 * 60 * 1000);

        req.file('avatar').upload({
            adapter: require('skipper-s3-resizer'),
            key: sails.config.s3Key,
            secret: sails.config.s3Secret,
            bucket: sails.config.s3Bucket,
            prefix: path.join(sails.config.environment !== 'production' ? 'devel/' + os.hostname() + '/' : '', 'user-avatars'),
            filename: function (base, ext) {
                return req.user.slug + ext;
            },
            resize: {
                width: 400
            },
            headers: {
                'x-amz-acl': 'public-read'
            }
        }, function (err, uploaded) {
            if (err || _.isUndefined(uploaded) || _.isUndefined(uploaded[0]))
                return res.serverError();

            var avatar = uploaded[0];

            User.update({id: req.user.id}, {avatarUrl: avatar.extra.Location}).exec(function (err) {
                if (err)
                    return res.serverError();

                return res.ok();
            });
        });
    },

    deleteAvatar: function (req, res) {
        User.update({id: req.user.id}, {avatarUrl: sails.config.properties.defaultAvatarUrl}).exec(function (err) {
            if (err)
                return res.serverError();

            return res.ok();
        })
    },

    changePassword: function (req, res) {
        if (!await(bcrypt.compare(req.body.currentPassword, req.user.password)) || req.body.newPassword !== req.body.newPasswordConfirmation) {
            return res.badRequest();
        }

        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(req.body.newPassword, salt, function (err, hash) {
                if (err) {
                    return res.serverError();
                }

                User.update({id: req.user.id}, {password: hash}).exec(function (err) {
                    if (err) {
                        return res.serverError();
                    }

                    return res.ok();
                });
            });
        });
    }
};