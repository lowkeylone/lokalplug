var _ = require('lodash'),
    moment = require('moment'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    uuidv1 = require('uuid/v1'),
    genId = require('gen-id')('XXXXXXXX'),
    fileExt = require('file-extension'),
    path = require('path'),
    slugify = require('slug'),
    os = require('os');

module.exports = {
    renderView: async(function (req, res) {
        if (req.user.isUploader === false && sails.config.disableUploadRequest === true)
            return res.notFound();

        var title = req.__('accountPage.uploadWizard.title');
        moment.locale(req.getLocale());

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'upload',
            title: title,
            moment: moment,
            musicGenres: await(MusicGenre.find()),
            selectLocale: {
                emptyTitle: req.__('accountPage.uploadWizard.searchSelect.emptyTitle'),
                currentlySelected: req.__('accountPage.uploadWizard.searchSelect.currentlySelected'),
                searchPlaceholder: req.__('accountPage.uploadWizard.searchSelect.searchPlaceholder'),
                statusInitialized: req.__('accountPage.uploadWizard.searchSelect.statusInitialized'),
                statusNoResults: req.__('accountPage.uploadWizard.searchSelect.statusNoResults'),
                statusSearching: req.__('accountPage.uploadWizard.searchSelect.statusSearching'),
            }
        };

        return res.view('accountUpload', locals);
    }),

    uploadCover: function (req, res) {
        if (!req._fileparser.upstreams.length)
            return res.badRequest();

        req.setTimeout(5 * 60 * 1000);

        req.file('cover').upload({
            adapter: require('skipper-s3-resizer'),
            key: sails.config.s3Key,
            secret: sails.config.s3Secret,
            bucket: sails.config.s3Bucket,
            prefix: path.join(sails.config.environment !== 'production' ? 'devel/' + os.hostname() + '/' : '', 'mixtape-covers'),
            filename: function (base, ext) {
                return uuidv1() + ext;
            },
            resize: {
                stretch: true,
                width: 500,
                height: 500
            },
            headers: {
                'x-amz-acl': 'public-read'
            }
        }, function (err, uploaded) {
            if (err || _.isUndefined(uploaded) || _.isUndefined(uploaded[0]))
                return res.serverError();

            var cover = uploaded[0];

            return res.ok({coverUrl: cover.extra.Location});
        });
    },

    uploadTrack: async(function (req, res) {
        if (!req._fileparser.upstreams.length)
            return res.badRequest();

        req.setTimeout(10 * 60 * 1000);

        var trackFilename = slugify(genId.generate() + '-' + req.body.mixtapeTitle + '-' + req.body.trackNumber + '-' + req.body.trackTitle);

        if (!_.isEmpty(req.body.trackArtist))
            trackFilename += slugify('-' + req.body.trackArtist);

        if (!_.isEmpty(req.body.trackMusicGroup))
            trackFilename += slugify('-' + req.body.trackMusicGroup);

        if (!_.isEmpty(req.body.trackFeaturing))
            trackFilename += slugify('-Feat-' + req.body.trackFeaturing);

        if (!_.isEmpty(req.body.trackBeatmakers))
            trackFilename += slugify('-Prod-' + req.body.trackBeatmakers);

        if (trackFilename.length > 95) {
            trackFilename = trackFilename.substring(0, 95);
        }

        req.file('track').upload({
            adapter: require('skipper-s3'),
            key: sails.config.s3Key,
            secret: sails.config.s3Secret,
            bucket: sails.config.s3Bucket,
            saveAs: function (fs, next) {
                var rootFolder = sails.config.environment !== 'production' ? 'devel/' + os.hostname() + '/' : '';
                next(null, path.join(rootFolder, 'mixtape-tracks', trackFilename + '.' + fileExt(fs.filename)));
            }
        }, function (err, uploaded) {
            if (err || _.isUndefined(uploaded) || _.isUndefined(uploaded[0]))
                return res.serverError();

            var track = uploaded[0];

            return res.ok({trackUrl: unescape(track.extra.Location)});
        });
    }),

    createMixtape: async(function (req, res) {
        moment.locale(req.getLocale());
        var mixtapePrefix = genId.generate();

        var mixtapeTypes = [];

        _.forEach(req.body.artists, function (artist) {
            if (await(Artist.count({id: artist.id, slug: artist.slug, type: 'artist'})) === 1
                || await(MusicGroup.count({id: artist.id, slug: artist.slug})) === 1) {
                mixtapeTypes = _.union(mixtapeTypes, ['artist-mixtape']);
            } else if (await(Artist.count({id: artist.id, slug: artist.slug, type: 'dj'})) === 1) {

                mixtapeTypes = _.union(mixtapeTypes, ['dj-mixtape']);
            } else if (await(Beatmaker.count({id: artist.id, slug: artist.slug})) === 1) {

                mixtapeTypes = _.union(mixtapeTypes, ['beatmaker-mixtape']);
            }
        });

        var createBody = {
            title: req.body.title,
            slug: mixtapePrefix + '-' + slugify(req.body.title, {lower: true}),
            coverUrl: req.body.coverUrl,
            releaseDate: moment(req.body.releaseDate, req.getLocale() === 'fr' ? 'DD/MM/YYYY' : 'MM/DD/YYYY').format('YYYY-MM-DD'),
            uploader: req.user.id,
            isOfficial: req.body.isOfficial,
            confirmationCode: genId.generate(),
            isConfirmed: req.body.isOfficial === 'true' ? 'false' : 'true'
        };

        var mixtape = await(Mixtape.create(createBody));

        _.forEach(mixtapeTypes, function (mixtapeType) {
            var type = await(MixtapeType.findOne({slug: mixtapeType}));

            if (_.isUndefined(type))
                return res.badRequest();

            type.mixtapes.add(mixtape.id);
            type.save();
        });

        _.forEach(req.body.artists, function (artist) {
            var owner = await(Artist.findOne({id: artist.id, slug: artist.slug}));

            if (_.isEmpty(owner))
                owner = await(MusicGroup.findOne({id: artist.id, slug: artist.slug}));

            if (_.isEmpty(owner))
                owner = await(Beatmaker.findOne({id: artist.id, slug: artist.slug}));

            if (_.isUndefined(owner))
                return res.badRequest();

            owner.ownMixtapes.add(mixtape.id);
            owner.save();
        });

        _.forEach(_.map(req.body.tracks, 'featuringArtists'), function (featuringArtists) {
            _.forEach(featuringArtists, function (artist) {
                if (_.isEmpty(artist))
                    return;

                var featuring = await(Artist.findOne({id: artist.id, slug: artist.slug}));

                if (_.isEmpty(featuring))
                    featuring = await(MusicGroup.findOne({id: artist.id, slug: artist.slug}));

                if (_.isUndefined(featuring))
                    return res.badRequest();

                featuring.featuringMixtapes.add(mixtape.id);
                featuring.save();
            });
        });

        _.forEach(_.map(req.body.tracks, 'beatmakers'), function (beatmakers) {
            _.forEach(beatmakers, function (beatmaker) {
                if (_.isEmpty(beatmaker))
                    return;

                var prod = await(Beatmaker.findOne({id: beatmaker.id}));

                if (_.isUndefined(prod))
                    return res.badRequest();

                if (_.isUndefined(prod.ownMixtapes) || _.indexOf(prod.ownMixtapes, mixtape.id) === -1) {
                    prod.featuringMixtapes.add(mixtape.id);
                    prod.save();
                }
            });
        });

        _.forEach(req.body.musicGenres, function (musicGenre) {
            var genre = await(MusicGenre.findOne({id: musicGenre.id}));

            if (_.isUndefined(genre))
                return res.badRequest();

            genre.mixtapes.add(mixtape.id);
            genre.save();
        });

        _.forEach(req.body.tracks, function (track) {
            var id = await(Track.create({
                title: track.title,
                slug: mixtapePrefix + '-' + slugify(track.title, {lower: true}),
                fileUrl: track.url,
                number: track.number,
                mixtape: mixtape.id,
                isOfficial: req.body.isOfficial
            })).id;

            var owner = await(Artist.findOne({id: track.artist.id, slug: track.artist.slug}));

            if (_.isEmpty(owner)) {
                owner = await(MusicGroup.findOne({
                    id: track.artist.id,
                    slug: track.artist.slug
                }));
            }

            if (_.isUndefined(owner))
                return res.badRequest();

            owner.ownTracks.add(id);
            owner.save();

            _.forEach(track.featuringArtists, function (artist) {
                var featuring = await(Artist.findOne({id: artist.id, slug: artist.slug}));

                if (_.isEmpty(featuring)) {
                    featuring = await(MusicGroup.findOne({id: artist.id, slug: artist.slug}));
                }

                if (_.isUndefined(featuring))
                    return res.badRequest();

                featuring.featuringTracks.add(id);
                featuring.save();
            });

            _.forEach(track.beatmakers, function (beatmaker) {
                var prod = await(Beatmaker.findOne({id: beatmaker.id}));

                if (_.isUndefined(prod))
                    return res.badRequest();

                prod.tracks.add(id);
                prod.save();
            });
        });

        uploadService.createMixtapeFile(mixtape.id, mixtapePrefix, slugify(req.body.title), req.body.coverUrl, req.body.tracks, req._sails.log);

        return res.ok(mixtape);
    })
}
;