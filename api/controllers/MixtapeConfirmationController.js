var moment = require('moment'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {

    renderUnconfirmedMixtapesView: function (req, res) {
        var title = 'Mixtapes non confirmées';

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'unconfirmedMixtapes'
        };

        return res.view('accountUnconfirmedMixtapes', locals);
    },

    getUnconfirmedMixtapes: function (req, res) {
        moment.locale('fr');

        Mixtape.find({isConfirmed: false})
            .populate('artists')
            .populate('musicGroups')
            .populate('beatmakers')
            .populate('uploader')
            .skip(req.query.skip)
            .limit(req.query.limit)
            .sort(req.query.order_by + ' ' + req.query.sort)
            .exec(function (err, mixtapes) {
                _.forEach(mixtapes, function (mixtape) {
                    mixtape.createdAt = moment(mixtape.createdAt).format('LL');
                });

                if (err)
                    return res.serverError();

                return res.ok(mixtapes);
            })
    },

    confirmMixtape: async(function (req, res) {
        var mixtape = await(Mixtape.findOne({slug: req.param('mixtapeSlug')})
            .populate('beatmakers')
            .populate('musicGroups')
            .populate('artists'));

        if (_.isEmpty(mixtape))
            return res.notFound();

        var authors = _.concat(mixtape.beatmakers, mixtape.musicGroups, mixtape.artists);

        mixtape.isConfirmed = true;
        await(mixtape.save());

        _.forEach(authors, function (author) {
            author.officialMixtapesCount++;

            if (_.isEmpty(author.lastOfficialActivityAt) || moment(mixtape.releaseDate).isAfter(author.lastOfficialActivityAt))
                author.lastOfficialActivityAt = mixtape.releaseDate;

            await(author.save());
        });

        var tracks = await(Track.find({mixtape: mixtape.id})
            .populate('artist')
            .populate('beatmakers')
            .populate('musicGroup'));

        _.forEach(tracks, function (track) {
            track.isConfirmed = true;
            await(track.save());

            if (!_.isEmpty(track.artist)) {
                var artist = await(Artist.findOne({id: track.artist.id}));

                artist.officialTracksCount++;

                if (_.isEmpty(artist.lastOfficialActivityAt) || moment(mixtape.releaseDate).isAfter(artist.lastOfficialActivityAt))
                    artist.lastOfficialActivityAt = mixtape.releaseDate;

                await(artist.save());
            }

            if (!_.isEmpty(track.musicGroup)) {
                var musicGroup = await(MusicGroup.findOne({id: track.musicGroup.id}));

                musicGroup.officialTracksCount++;

                if (_.isEmpty(musicGroup.lastOfficialActivityAt) || moment(mixtape.releaseDate).isAfter(musicGroup.lastOfficialActivityAt))
                    musicGroup.lastOfficialActivityAt = mixtape.releaseDate;

                await(musicGroup.save());
            }

            _.forEach(track.beatmakers, function (beatmaker) {
                beatmaker = await(Beatmaker.findOne({id: beatmaker.id}));

                beatmaker.officialTracksCount++;

                if (_.isEmpty(beatmaker.lastOfficialActivityAt) || moment(mixtape.releaseDate).isAfter(beatmaker.lastOfficialActivityAt))
                    beatmaker.lastOfficialActivityAt = mixtape.releaseDate;

                await(beatmaker.save());
            });
        });

        res.ok();
    })
};