var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    slugify = require('slug'),
    moment = require('moment'),
    URI = require('urijs');

module.exports = {
    renderAllArtistsView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var locals = {
            pageTitle: req.__('artistsPage.title') + ' - Lokal Plug',
            section: 'artist',
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment
        };

        locals.filters = {
            todayDate: moment().format('YYYY-MM-DD'),
            labels: {
                type: [
                    {
                        label: 'onlyArtists',
                        query: 'artist',
                        value: 'artist'
                    },
                    {
                        label: 'onlyDJs',
                        query: 'dj',
                        value: 'dj'
                    }
                ]
            }
        };

        var filters = queryService.filterListQuery(locals.filters, req.query);
        filters.lastOfficialActivityAt = {'!': null};

        var data = {};

        data.totalPages = parseInt(await(Artist.count(filters)) / 24) + 1;
        data.currentPage = req.query.page || 1;
        data.previous = data.currentPage - 1 > 0;
        data.next = data.currentPage < data.totalPages;

        if (data.currentPage > data.totalPages) {
            var url = new URI(req.url).removeSearch('page').addSearch('page', data.totalPages);
            return res.redirect(url.toString());
        }

        data.results = await(Artist.find(filters)
            .paginate({page: data.currentPage, limit: 24})
            .populate('ownMixtapes'));

        _.forEach(data.results, function (artist, index) {
            var rate = 0;

            _.forEach(artist.ownMixtapes, function (mixtape) {
                rate = rate + mixtape.rate;
            });

            artist.rate = rate;

            data.results[index] = artist;
        });

        data.results = _.sortBy(data.results, [function (artist) {
            return moment(artist.lastOfficialActivityAt).valueOf();
        }, "rate"]).reverse();

        data.filters = {};

        if (req.query.type) {
            data.filters.type = req.query.type;
        }

        locals.data = data;

        return res.view('artists', locals);
    }),

    renderArtistView: async(function (req, res) {
        moment.locale(req.getLocale());

        var weekMostDownloadedIds = await(mixtapeService.getWeekMostDownloadedIds(3));

        var artist = await(Artist.findOne({slug: req.param('artistSlug')}));

        if (_.isUndefined(artist))
            return res.notFound();

        var section = 'artist';

        var locals = {
            pageTitle: artist.name + ' - Lokal Plug',
            section: section,
            weekMostDownloadedMixtapes: await(Mixtape.find({id: weekMostDownloadedIds, isConfirmed: true}).sort('downloadCount desc')),
            moment: moment,
            artist: artist,
            ownMixtapes: await(Mixtape.find({
                id: await(mixtapeService.getIdsByOwner(artist.slug, 'artist')),
                isConfirmed: true
            }).populate('artists').populate('beatmakers').populate('musicGroups').populate('musicGenres')),
            featuringMixtapes: await(Mixtape.find({
                id: await(mixtapeService.getIdsByFeaturing(artist.slug, 'artist')),
                isConfirmed: true
            }).populate('artists').populate('beatmakers').populate('musicGroups').populate('musicGenres'))
        };

        return res.view('artist', locals);
    }),

    renderAllArtistsAdminView: function (req, res) {
        var title = 'Tous les artistes';

        var locals = {
            pageTitle: title + ' - Lokal Plug',
            section: 'other',
            accountNav: 'allArtists'
        };

        return res.view('accountAllArtists', locals);
    },

    getAllArtists: function (req, res) {
        moment.locale('fr');

        Artist.find()
            .populate('musicGroups')
            .populate('owners')
            .skip(req.query.skip)
            .limit(req.query.limit)
            .sort(req.query.order_by + ' ' + req.query.sort)
            .exec(function (err, artists) {
                _.forEach(artists, function (artist) {
                    artist.lastOfficialActivityAt = moment(artist.lastOfficialActivityAt).format('LL');
                    artist.createdAt = moment(artist.createdAt).format('LL');
                });

                if (err)
                    return res.serverError();

                return res.ok(artists);
            });
    },

    findAllByName: async(function (req, res) {
        if (_.isEmpty(req.query.q))
            return res.badRequest();

        var query = req.query.q;

        var artists = await(Artist.find({name: {startsWith: query}, type: 'artist'}).limit(3));
        var djs = await(Artist.find({name: {startsWith: query}, type: 'dj'}).limit(3));
        var beatmakers = await(Beatmaker.find({name: {startsWith: query}}).limit(3));
        var musicGroups = await(MusicGroup.find({name: {startsWith: query}}).limit(3));

        return res.status(200).send({
            artists: artists,
            djs: djs,
            beatmakers: beatmakers,
            musicGroups: musicGroups
        });
    }),

    findAllExceptGroupsByName: async(function (req, res) {
        if (_.isEmpty(req.query.q))
            return res.badRequest();

        var query = req.query.q;

        var artists = await(Artist.find({name: {startsWith: query}, type: 'artist'}).limit(3));
        var djs = await(Artist.find({name: {startsWith: query}, type: 'dj'}).limit(3));
        var beatmakers = await(Beatmaker.find({name: {startsWith: query}}).limit(3));

        return res.status(200).send({
            artists: artists,
            djs: djs,
            beatmakers: beatmakers
        });
    }),

    findAllExceptBeatmakers: async(function (req, res) {
        if (_.isEmpty(req.query.q))
            return res.badRequest();

        var query = req.query.q;

        var artists = await(Artist.find({name: {startsWith: query}, type: 'artist'}).limit(3));
        var djs = await(Artist.find({name: {startsWith: query}, type: 'dj'}).limit(3));
        var musicGroups = await(MusicGroup.find({name: {startsWith: query}}).limit(3));

        return res.status(200).send({
            artists: artists,
            djs: djs,
            musicGroups: musicGroups
        });
    }),

    findBeatmakersByName: async(function (req, res) {
        if (_.isEmpty(req.query.q))
            return res.badRequest();

        var query = req.query.q;

        var beatmakers = await(Beatmaker.find({name: {startsWith: query}}).limit(3));

        return res.status(200).send({
            beatmakers: beatmakers
        });
    }),

    findMusicGroupsByName: async(function (req, res) {
        if (_.isEmpty(req.query.q))
            return res.badRequest();

        var query = req.query.q;

        var musicGroups = await(MusicGroup.find({name: {startsWith: query}}).limit(3));

        return res.status(200).send({
            musicGroups: musicGroups
        });
    }),

    validateName: async(function (req, res) {
        if (!_.isUndefined(req.query.name)) {
            var artistsCount = await(Artist.count({slug: slugify(req.query.name, {lower: true})}));
            var beatmakersCount = await(Beatmaker.count({slug: slugify(req.query.name, {lower: true})}));
            var musicGroupsCount = await(MusicGroup.count({slug: slugify(req.query.name, {lower: true})}));

            if (artistsCount + beatmakersCount + musicGroupsCount > 0) {
                res.writeHead(400, req.__('accountPage.uploadWizard.modals.createArtistModal.artistNameAlreadyExist'));
                return res.send();
            }
        }

        return res.ok();
    }),

    createArtist: async(function (req, res) {
        req.body.slug = slugify(req.body.name, {lower: true});

        var musicGroupIds = req.body.musicGroups;
        _.unset(req.body, 'musicGroups');

        var artist = await(Artist.create(req.body));
        var adminUsers = await(User.find({isAdmin: true}));
        var musicGroups = await(MusicGroup.find({id: musicGroupIds}));

        _.forEach(adminUsers, function (admin) {
            admin.artists.add(artist.id);
            admin.save(function (err) {
            });
        });

        _.forEach(musicGroups, function (musicGroup) {
            musicGroup.artists.add(artist.id);
            musicGroup.save(function (err) {
            });
        });

        return res.ok();
    }),

    deleteArtist: async(function (req, res) {
        var artist = await(Artist.findOne({slug: req.param('artistSlug')})
            .populate('ownTracks')
            .populate('featuringTracks')
            .populate('ownMixtapes')
            .populate('featuringMixtapes')
            .populate('musicGroups')
            .populate('owners'));

        if (_.isEmpty(artist))
            return res.notFound();

        if (!_.map(artist.owners, 'id').includes(req.user.id) && !req.user.isAdmin)
            return res.badRequest();

        if (!_.isEmpty(artist.ownMixtapes) || !_.isEmpty(artist.featuringMixtapes)
            || !_.isEmpty(artist.ownTracks) || !_.isEmpty(artist.featuringTracks))
            return res.badRequest('artist with id \'' + artist.id + '\' has at least one relation with a mixtape, please delete it first');

        artistService.removeArtistRelations(artist);

        Artist.destroy({slug: req.param('artistSlug')}).exec(function (err) {
            if (err)
                return res.serverError();

            return res.ok();
        });
    })
};