var _ = require('lodash');

module.exports = {
    switchLang: function(req, res) {
        req.session.lplang = req.param('language');
        res.ok();
    }
};