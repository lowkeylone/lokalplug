module.exports = {
    renderCopyrightPolicyView: function(req, res) {
        var locals = {
            pageTitle: 'Copyright Policy - Lokal Plug',
            section: 'other'
        };

        return res.view('copyrightPolicy', locals);
    },

    renderPrivacyPolicyView: function(req, res) {
        var locals = {
            pageTitle: 'Privacy Policy - Lokal Plug',
            section: 'other'
        };

        return res.view('privacyPolicy', locals);
    },

    renderTermsOfUseView: function(req, res) {
        var locals = {
            pageTitle: 'Terms of use - Lokal Plug',
            section: 'other'
        };

        return res.view('termsOfUse', locals);
    }
};