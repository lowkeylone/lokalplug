var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
    getFileUrl: async(function (req, res) {
        var track = await(Track.findOne({slug: req.param('trackSlug')}));

        if (_.isUndefined(track) || _.isEmpty(track.fileUrl))
            return res.notFound();

        return res.ok({fileUrl: track.fileUrl});
    }),

    downloadTrack: async(function (req, res) {
        var track = await(Track.findOne({slug: req.param('trackSlug')}));

        if (_.isUndefined(track) || _.isEmpty(track.fileUrl))
            return res.notFound();

        await(Track.update({id: track.id}, {downloadCount: (track.downloadCount + 1)}));

        return res.redirect(uploadService.convertS3Url(track.fileUrl));
    }),

    getRandomTracks: function (req, res) {
        Track.native(function (err, track) {
            track.aggregate([{'$sample': {'size': 10}}], function (err, result) {
                if (err)
                    return res.serverError();

                Track.find().where({id: _.map(result, '_id')})
                    .populate('mixtape')
                    .populate('artist')
                    .populate('featuringArtists')
                    .populate('musicGroup')
                    .populate('featuringMusicGroups')
                    .populate('beatmakers')
                    .exec(function (err, tracks) {
                        if (err)
                            return res.serverError();

                        return res.ok(tracks);
                });
            });
        });
    }
};