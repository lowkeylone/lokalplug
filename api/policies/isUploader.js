module.exports = function(req, res, next) {
    if (req.isAuthenticated() && req.user.isUploader === true) {
        return next();
    }
    else{
        return res.notFound();
    }
};