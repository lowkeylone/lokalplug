module.exports = function(req, res, next) {
    if (!_.isEmpty(req.session.lplang))
        req.setLocale(req.session.lplang);

    Language.find().exec(function (err, found) {
        var languages = {};

        _.forEach(found, function (language) {
            languages[language.languageCode] = language;
        });

        req.languages = languages;

        next();
    });
};