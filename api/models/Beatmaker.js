module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        pictureUrl: {
            type: 'text',
            defaultsTo: sails.config.properties.defaultBeatmakerPictureUrl
        },
        coverUrl: {
            type: 'text',
            defaultsTo: sails.config.properties.defaultArtistCoverUrl
        },
        lastOfficialActivityAt: {
            type: 'date'
        },
        officialMixtapesCount: {
            type: 'integer',
            defaultsTo: 0
        },
        officialTracksCount: {
            type: 'integer',
            defaultsTo: 0
        },
        tracks: {
            collection: 'track',
            via: 'beatmakers',
            dominant: true
        },
        ownMixtapes: {
            collection: 'mixtape',
            via: 'beatmakers',
            dominant: true
        },
        featuringMixtapes: {
            collection: 'mixtape',
            via: 'featuringBeatmakers',
            dominant: true
        },
        musicGroups: {
            collection: 'musicGroup',
            via: 'beatmakers'
        },
        owners: {
            collection: 'user',
            via: 'beatmakers'
        }
    }
};