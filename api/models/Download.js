module.exports = {
    attributes: {
        mixtape: {
            model: 'mixtape',
            required: true
        },
        date: {
            type: 'date',
            required: true
        },
        user: {
            model: 'user'
        }
    }
};