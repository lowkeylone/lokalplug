module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        mixtapes: {
            collection: 'mixtape',
            via: 'musicGenres',
            dominant: true
        },
        color: {
            type: 'string',
            required: true,
            unique: true
        }
    }
};