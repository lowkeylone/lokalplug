module.exports = {
    attributes: {
        title: {
            type: 'string',
            required: 'true'
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        fileUrl: {
            type: 'text',
            required: true
        },
        mixtape: {
            model: 'mixtape'
        },
        number: {
            type: 'integer',
            defaultsTo: 1
        },
        artist: {
            model: 'artist'
        },
        featuringArtists: {
            collection: 'artist',
            via: 'featuringTracks'
        },
        musicGroup: {
            model: 'musicGroup'
        },
        featuringMusicGroups: {
            collection: 'musicGroup',
            via: 'featuringTracks'
        },
        beatmakers: {
            collection: 'beatmaker',
            via: 'tracks'
        },
        downloadCount: {
            type: 'integer',
            defaultsTo: 0
        },
        playCount: {
            type: 'integer',
            defaultsTo: 0
        },
        upcoming: {
            type: 'boolean',
            defaultsTo: false
        },
        isOfficial: {
            type: 'boolean',
            defaultsTo: false
        },
        isConfirmed: {
            type: 'boolean',
            defaultsTo: false
        }
    }
};