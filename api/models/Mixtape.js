module.exports = {
    attributes: {
        title: {
            type: 'string',
            required: true
        },
        slug: {
            type: 'string',
            required: true,
            unique: true
        },
        types: {
            collection: 'mixtapeType',
            via: 'mixtapes'
        },
        coverUrl: {
            type: 'text',
            required: true
        },
        musicGroups: {
            collection: 'musicGroup',
            via: 'ownMixtapes'
        },
        featuringMusicGroups: {
            collection: 'musicGroup',
            via: 'featuringMixtapes'
        },
        artists: {
            collection: 'artist',
            via: 'ownMixtapes'
        },
        featuringArtists: {
            collection: 'artist',
            via: 'featuringMixtapes'
        },
        beatmakers: {
            collection: 'beatmaker',
            via: 'ownMixtapes'
        },
        featuringBeatmakers: {
            collection: 'beatmaker',
            via: 'featuringMixtapes'
        },
        musicGenres: {
            collection: 'musicGenre',
            via: 'mixtapes'
        },
        downloadCount: {
            type: 'integer',
            defaultsTo: 0
        },
        downloads: {
            collection: 'download',
            via: 'mixtape'
        },
        rate: {
            type: 'float',
            defaultsTo: 0.0
        },
        releaseDate: {
            type: 'date',
            required: true
        },
        uploader: {
            model: 'user',
            required: true
        },
        bookmarkedBy: {
            collection: 'user',
            via: 'mixtape',
            through: 'usermixtapebookmark'
        },
        likedBy: {
            collection: 'user',
            via: 'mixtape',
            through: 'usermixtapelike'
        },
        dislikedBy: {
            collection: 'user',
            via: 'mixtape',
            through: 'usermixtapedislike'
        },
        tracks: {
            collection: 'track',
            via: 'mixtape'
        },
        isOfficial: {
            type: 'boolean',
            defaultsTo: false
        },
        upcoming: {
            type: 'boolean',
            defaultsTo: false
        },
        confirmationCode: {
            type: 'text'
        },
        isConfirmed: {
            type: 'boolean',
            required: true
        },
        isFileAvailable: {
            type: 'boolean',
            defaultsTo: false
        },
        fileUrl: {
            type: 'text'
        }
    }
};