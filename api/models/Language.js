module.exports = {
    attributes: {
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        languageCode: {
            type: 'string',
            required: true,
            unique: true
        },
        countryCode: {
            type: 'string',
            required: true,
            unique: true
        },
        localeCode: {
            type: 'string',
            required: true,
            unique: true
        }
    }
};