var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
    removeArtistRelations: async(function (artist) {
        _.forEach(artist.musicGroups, function (musicGroup) {
            musicGroup.artists.remove(artist.id);
            await(musicGroup.save());
        });

        _.forEach(artist.owners, function (owner) {
            owner.artists.remove(artist.id);
            await(owner.save());
        });
    })
};