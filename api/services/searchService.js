var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await');

module.exports = {
    searchMixtapes: async(function (q) {
        return await(Mixtape.find({
            title: {'contains': q},
            isConfirmed: true
        }).populate('artists').populate('beatmakers').populate('musicGroups').populate('musicGenres'));
    }),

    searchArtists: async(function (q) {
        return await(Artist.find({
            name: {'contains': q},
            type: 'artist'
        }));
    }),

    searchDJs: async(function (q) {
        return await(Artist.find({
            name: {'contains': q},
            type: 'dj'
        }));
    }),

    searchBeatmakers: async(function (q) {
        return await(Beatmaker.find({
            name: {'contains': q}
        }));
    }),

    searchMusicGroups: async(function (q) {
        return await(MusicGroup.find({
            name: {'contains': q}
        }));
    })
};