var _ = require('lodash'),
    async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    moment = require('moment');

module.exports = {
    getById: async(function (id) {
        return await(Mixtape.findOne({id: id})
            .populate('artists')
            .populate('beatmakers')
            .populate('musicGroups')
            .populate('musicGenres'));
    }),

    updateRate: async(function (slug) {
        var mixtape = await(Mixtape.findOne({slug: slug}));

        var likeCount = await(UserMixtapeLike.count({mixtape: mixtape.id}));
        var dislikeCount = await(UserMixtapeDislike.count({mixtape_dislikedBy: mixtape.id}));

        var rate;

        if (likeCount === 0 && dislikeCount === 0) {
            rate = 0.0;
        } else {
            var likePercent = 100 * likeCount / (likeCount + dislikeCount);

            rate = 5 * likePercent / 100;
        }

        await(Mixtape.update({id: mixtape.id}, {rate: rate}));

        return rate;
    }),

    getTrendingIds: async(function (limit) {
        var date = moment().subtract(1, 'day').startOf('month');

        var ids = [];

        for(var i = 0; i < 3 && ids.length < limit; i++) {
            _.forEach(await(this.getMostDownloadedIdsByDate(date.format("YYYY-MM-DD"), (limit-ids.length), ids)), function (id) {
                if (_.indexOf(ids, id) === -1) {
                    ids.push(id);
                }
            });

            date.subtract(1, 'months');
        }

        return ids;
    }),

    getWeekMostDownloadedIds: async(function (limit) {

        var lastWeek = moment()
            .startOf('week');

        var ids = [];

        for(var i = 0; i < 3 && ids.length < limit; i++) {
            _.forEach(await(this.getMostDownloadedIdsByDate(lastWeek.format("YYYY-MM-DD"), (limit-ids.length), ids)), function (id) {
                if (_.indexOf(ids, id) === -1) {
                    ids.push(id);
                }
            });

            lastWeek.subtract(7, 'days');
        }

        return ids;
    }),

    getMostDownloadedIdsByDate: async(function (date, limit, excludedIds) {
        var counts = [];

        var downloads = await(Download.find({date: { '>': date, '<': moment().format() }}));

        _.forEach(downloads, function(download) {
            if (_.indexOf(excludedIds, download.mixtape) === -1) {
                if (_.isUndefined(_.find(counts, {mixtapeId: download.mixtape}))) {
                    counts.push({
                        mixtapeId: download.mixtape,
                        downloads: 1
                    })
                } else {
                    _.find(counts, {mixtapeId: download.mixtape}).downloads++;
                }
            }
        });

        counts = _.orderBy(counts, 'downloads', 'desc');

        var mostDownloadedMixtapes = [];

        for (var i = 0; i < limit && counts.length > 0; i++) {
            mostDownloadedMixtapes.push(_.head(counts).mixtapeId);
            _.pullAt(counts, 0);
        }

        return mostDownloadedMixtapes;
    }),

    getIdsByOwner: async(function (slug, type) {
        var artistId;

        if (type === 'artist') {
            artistId = await(Artist.findOne({slug: slug})).id;

            return _.map(await(sails.models['artist_ownmixtapes__mixtape_artists'].find({artist_ownMixtapes: artistId})), 'mixtape_artists');
        }
        else if (type === 'beatmaker') {
            artistId = await(Beatmaker.findOne({slug: slug})).id;

            return _.map(await(sails.models['beatmaker_ownmixtapes__mixtape_beatmakers'].find({beatmaker_ownMixtapes: artistId})), 'mixtape_beatmakers');
        }
        else {
            artistId = await(MusicGroup.findOne({slug: slug})).id;

            return _.map(await(sails.models['mixtape_musicgroups__musicgroup_ownmixtapes'].find({musicgroup_ownMixtapes: artistId})), 'mixtape_musicGroups');
        }
    }),

    getIdsByFeaturing: async(function (slug, type) {
        var artistId;

        if (type === 'artist') {
            artistId = await(Artist.findOne({slug: slug})).id;

            return _.map(await(sails.models['artist_featuringmixtapes__mixtape_featuringartists'].find({artist_featuringMixtapes: artistId})), 'mixtape_featuringArtists');
        }
        else if (type === 'beatmaker') {
            artistId = await(Beatmaker.findOne({slug: slug})).id;

            return _.map(await(sails.models['beatmaker_featuringmixtapes__mixtape_featuringbeatmakers'].find({beatmaker_featuringMixtapes: artistId})), 'mixtape_featuringBeatmakers');
        }
        else {
            artistId = await(MusicGroup.findOne({slug: slug})).id;

            return _.map(await(sails.models['mixtape_featuringmusicgroups__musicgroup_featuringmixtapes'].find({musicgroup_featuringMixtapes: artistId})), 'mixtape_featuringMusicGroups');
        }
    }),

    getBookmarkedIdsByUserId: async(function (userId, limit) {
        return _.map(await(UserMixtapeBookmark.find({where: {user: userId}, limit: limit, sort: 'createdAt DESC'})), 'mixtape');
    }),

    getUploadedMixtapeByUserId: async(function (userId, limit) {
        return await(Mixtape.find({where: {uploader: userId}, limit: limit, sort: 'createdAt DESC'})
            .populate('artists')
            .populate('beatmakers')
            .populate('musicGroups')
            .populate('musicGenres'));
    }),

    removeMixtapeRelations: async(function (mixtape) {
        _.forEach(mixtape.types, function (type) {
            type.mixtapes.remove(mixtape.id);
            await(type.save());
        });

        _.forEach(mixtape.musicGroups, function (musicGroup) {
            musicGroup.ownMixtapes.remove(mixtape.id);

            if (mixtape.isOfficial === true && mixtape.isConfirmed === true) {
                musicGroup.officialMixtapesCount = musicGroup.officialMixtapesCount - 1;
                musicGroup.officialTracksCount = musicGroup.officialTracksCount - mixtape.tracks.length;
            }

            await(musicGroup.save());
        });

        _.forEach(mixtape.featuringMusicGroups, function (musicGroup) {
            musicGroup.featuringMixtapes.remove(mixtape.id);
            await(musicGroup.save());
        });

        _.forEach(mixtape.artists, function (artist) {
            artist.ownMixtapes.remove(mixtape.id);

            if (mixtape.isOfficial === true && mixtape.isConfirmed === true)
                artist.officialMixtapesCount--;

            await(artist.save());
        });

        _.forEach(mixtape.featuringArtists, function (artist) {
            artist.featuringMixtapes.remove(mixtape.id);
            await(artist.save());
        });

        _.forEach(mixtape.beatmakers, function (beatmaker) {
            beatmaker.ownMixtapes.remove(mixtape.id);

            if (mixtape.isOfficial === true && mixtape.isConfirmed === true) {
                beatmaker.officialMixtapesCount = beatmaker.officialMixtapesCount - 1;
                beatmaker.officialTracksCount = beatmaker.officialTracksCount - mixtape.tracks.length;
            }

            await(beatmaker.save());
        });

        _.forEach(mixtape.featuringBeatmakers, function (beatmaker) {
            beatmaker.featuringMixtapes.remove(mixtape.id);
            await(beatmaker.save());
        });

        _.forEach(mixtape.musicGenres, function (musicGenre) {
            musicGenre.mixtapes.remove(mixtape.id);
            await(musicGenre.save());
        });

        var uploader = await(User.findOne({id: mixtape.uploader})
            .populate('mixtapesUploaded'));

        _.pullAt(uploader.mixtapesUploaded, _.indexOf(_.map(uploader.mixtapesUploaded, 'id'), mixtape.id));
        await(uploader.save());

        _.forEach(mixtape.bookmarkedBy, function (user) {
            user.mixtapesBookmarked.remove(mixtape.id);
            await(user.save());
        });

        _.forEach(mixtape.likedBy, function (user) {
            user.mixtapesLiked.remove(mixtape.id);
            await(user.save());
        });

        _.forEach(mixtape.dislikedBy, function (user) {
            user.mixtapesDisliked.remove(mixtape.id);
            await(user.save());
        });
    })
};