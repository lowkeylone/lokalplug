var nodemailer = require('nodemailer'),
    pug = require('pug'),
    fs = require('fs'),
    path = require('path');

var noReplyTransporter = nodemailer.createTransport({
    service: "Zoho",
    auth: {
        user: 'no-reply@lokalplug.com',
        pass: sails.config.properties.noReplyMailPassword
    }
});

exports.sendNoReplyMail = function(receiver, subject, body) {
    var mailOptions = {
        from: '"Lokal Plug" <no-reply@lokalplug.com>',
        to: receiver,
        subject: subject,
        html: body
    };

    noReplyTransporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }

        console.log('NoReply mail sent: ' + info.response + ' (to: ' + receiver + ')');
    });
};

exports.compileTemplate = function(template, locals, next) {
    var templatePath = path.join(sails.config.paths.views, 'mails', template+'.pug');

    pug.renderFile(templatePath, locals, function (err, renderedTemplate) {
        if (err) {
            console.log(err);
        }

        next(null, renderedTemplate);
    });
};