var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
    removeTrackRelations: async(function(track) {
        var mixtape = await(Mixtape.findOne({id: track.mixtape}));

        if (!_.isEmpty(track.artist)) {
            var artist = await(Artist.findOne({id: track.artist})
                .populate('ownTracks'));

            _.pullAt(artist.ownTracks, _.indexOf(_.map(artist.ownTracks, 'id'), track.id));

            if (mixtape.isOfficial === true && mixtape.isConfirmed === true)
                artist.officialTracksCount--;

            await(artist.save());
        }

        if (!_.isEmpty(track.musicGroup)) {
            var musicGroup = await(MusicGroup.findOne({id: track.musicGroup})
                .populate('ownTracks'));

            _.pullAt(musicGroup.ownTracks, _.indexOf(_.map(musicGroup.ownTracks, 'id'), track.id));

            if (mixtape.isOfficial === true && mixtape.isConfirmed === true)
                musicGroup.officialTracksCount--;

            await(musicGroup.save());
        }

        _.forEach(track.featuringArtists, function (artist) {
            artist.featuringTracks.remove(track.id);
            await(artist.save());
        });

        _.forEach(track.featuringMusicGroups, function (musicGroup) {
            musicGroup.featuringTracks.remove(track.id);
            await(musicGroup.save());
        });

        _.forEach(track.beatmakers, function (beatmaker) {
            beatmaker.tracks.remove(track.id);

            if (mixtape.isOfficial === true && mixtape.isConfirmed === true)
                beatmaker.officialTracksCount--;

            await(beatmaker.save());
        });
    })
};