var async = require('asyncawait/async'),
    await = require('asyncawait/await');

module.exports = {
    asyncForEach: async(function (array, callback) {
        for (var index = 0; index < array.length; index++) {
            await(callback(array[index], index, array));
        }
    })
};