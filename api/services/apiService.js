var async = require('asyncawait/async'),
    await = require('asyncawait/await'),
    _ = require('lodash');

module.exports = {
   checkAdminKey: async(function (key) {
        if (_.isEmpty(key))
            return false;

        var user = await(User.findOne({apiKey: key, isAdmin: true}));

        return !_.isEmpty(user);
    })
};