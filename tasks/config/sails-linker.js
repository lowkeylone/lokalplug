var genId = require('gen-id')('nnnn');

module.exports = function (grunt) {

    var buildNumber = genId.generate();

    grunt.config.set('sails-linker', {
        devJsPug: {
            options: {
                startTag: '// SCRIPTS',
                endTag: '// SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': require('../pipeline').jsFilesToInject
            }
        },

        prodJsPug: {
            options: {
                startTag: '// SCRIPTS',
                endTag: '// SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': ['.tmp/public/min/prod.min.js']
            }
        },

        devJsDepPug: {
            options: {
                startTag: '// DEPENDENCIES SCRIPTS',
                endTag: '// DEPENDENCIES SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': require('../pipeline').jsDepFilesToInject
            }
        },

        prodJsDepPug: {
            options: {
                startTag: '// DEPENDENCIES SCRIPTS',
                endTag: '// DEPENDENCIES SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': ['.tmp/public/min/prod-dependencies.min.js']
            }
        },

        devJsUploadWizardPug: {
            options: {
                startTag: '// UPLOAD WIZARD SCRIPTS',
                endTag: '// UPLOAD WIZARD SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': require('../pipeline').jsUploadWizardFilesToInject
            }
        },

        prodJsUploadWizardPug: {
            options: {
                startTag: '// UPLOAD WIZARD SCRIPTS',
                endTag: '// UPLOAD WIZARD SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': ['.tmp/public/min/prod-upload-wizard.min.js']
            }
        },

        devJsAdminPug: {
            options: {
                startTag: '// ADMIN SCRIPTS',
                endTag: '// ADMIN SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': require('../pipeline').jsAdminFilesToInject
            }
        },

        prodJsAdminPug: {
            options: {
                startTag: '// ADMIN SCRIPTS',
                endTag: '// ADMIN SCRIPTS END',
                fileTmpl: 'script(src="%s?v=' + buildNumber + '")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': ['.tmp/public/min/prod-admin.min.js']
            }
        },

        devStylesPug: {
            options: {
                startTag: '// STYLES',
                endTag: '// STYLES END',
                fileTmpl: 'link(href="%s?v=' + buildNumber + '", rel="stylesheet")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': require('../pipeline').cssFilesToInject
            }
        },

        prodStylesPug: {
            options: {
                startTag: '// STYLES',
                endTag: '// STYLES END',
                fileTmpl: 'link(href="%s?v=' + buildNumber + '", rel="stylesheet")',
                appRoot: '.tmp/public'
            },
            files: {
                'views/**/*.pug': ['.tmp/public/min/prod.min.css']
            }
        }
    });

    grunt.loadNpmTasks('grunt-sails-linker');
};
